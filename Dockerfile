FROM openjdk:11
WORKDIR /app
COPY target/rautenback-*.jar ./
CMD java -jar rautenback-*.jar --port 80 --properties /mnt/props/secret.properties
