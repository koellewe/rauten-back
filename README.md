# Rautenback

Backend for rauten.co.za.


## Deployment

### JAR

```sh
java -jar rautenback-*.jar --port [port] --properties [propfile]
```

### Docker

```sh
docker run -dp [port]:80 -v [properties_dir]:/mnt/props --net [network] koellewe/rauten-back
```

## Dev

```sh
mvn exec:java
```

The above command uses port 8081 and reads "dev.properties". These args are hardcoded 
in `pom.xml` and can be overridden from the command line with `-Dexec.args="--port 
[port] --properties [propfile]"`. 

### IntelliJ

If IntelliJ idea is giving you weird Java interpretation issues (especially with JSON objects), run the following:

```sh
mvn idea:idea
```

### Config and such

For now, DB and propfile structure will only be made available on request. 
