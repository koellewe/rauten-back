package za.co.rauten.rautenback.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Technology {

	@JsonProperty
	private String slug;
	@JsonProperty
	private String alt;
	@JsonProperty
	private String link;
	@JsonProperty
	private String img;
	
	public Technology(String slug, String alt, String link, String img) {
		this.slug = slug;
		this.alt = alt;
		this.link = link;
		this.img = img;
	}

	public String getSlug() {
		return slug;
	}

	public String getAlt() {
		return alt;
	}

	public String getLink() {
		return link;
	}

	public String getImg() {
		return img;
	}
		
}
