package za.co.rauten.rautenback.data;


import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Project{
    @JsonProperty
    private int id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String status;
    @JsonProperty
    private Date lastUpdated;
    @JsonProperty
    private String description;
    
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<Technology> techs;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String techSlugs;
    
    @JsonProperty
    private String image;
    @JsonProperty
    private String website;
    @JsonProperty
    private String playstore;

    private Integer loc;
    // jsonProperty defined as function
    private VCSInfo vcs = null;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private int vcsID;

    public Project(int id, String name, String status, java.sql.Date lastUpdated, String description, List<Technology> techs,
                   String image, String website, String playstore, Integer loc) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.lastUpdated = lastUpdated==null ? null : new Date(lastUpdated.getTime());
        this.description = description;
        this.techs = techs;
        this.image = image;
        this.website = website;
        this.playstore = playstore;
        this.loc = loc; // nullable
    }

    public void setVcs(VCSInfo vcs) {
        this.vcs = vcs;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public String getDescription() {
        return description;
    }

    public String getTechSlugs() {
		return techSlugs;
	}

	public void setTechSlugs(String techSlugs) {
		this.techSlugs = techSlugs;
	}

	public List<Technology> getTechs() {
		return techs;
	}

	public String getImage() {
        return image;
    }

    public String getWebsite() {
        return website;
    }

    public String getPlaystore() {
        return playstore;
    }

    public VCSInfo getVcs() {
        return vcs;
    }
    public int getVcsID(){
        return vcsID;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY, value = "loc")
    public Integer getLoc(){
        return loc; // nullable
    }

    @JsonProperty(value = "vcs", access = JsonProperty.Access.READ_ONLY)
    public Map<String, Object> getVcsJson(){
        return this.getVcs() == null ? null : this.getVcs().abridge().toMap();
    }
}
