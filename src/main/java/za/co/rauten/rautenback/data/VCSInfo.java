package za.co.rauten.rautenback.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONObject;

public class VCSInfo {
    @JsonProperty
    private String provider;
    @JsonProperty
    private String user;
    @JsonProperty
    private String repo;
    @JsonProperty
    private boolean repoGroup;
    @JsonProperty
    private boolean isPrivate;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int id;

    public VCSInfo(int id, String provider, String user, String repo, boolean repoGroup, boolean isPublic) {
        this.id = id;
        this.provider = provider;
        this.user = user;
        this.repo = repo;
        this.repoGroup = repoGroup;
        this.isPrivate = !isPublic;
    }

    public String getProvider() {
        return provider;
    }

    public String getUser() {
        return user;
    }

    public String getRepo() {
        return repo;
    }

    public boolean isRepoGroup() {
        return repoGroup;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    /**
     * Abridged version of this class's data
     */
    public JSONObject abridge(){
        return new JSONObject()
                .put("weblink", this.getWebLink())
                .put("public", !this.isPrivate())
                .put("id", this.id);
    }

    @JsonProperty(value="weblink", access= JsonProperty.Access.READ_ONLY)
    public String getWebLink(){
        switch (this.getProvider()){
            case "bitbucket":
                if (this.isRepoGroup())
                    return String.format("https://bitbucket.org/account/user/%s/projects/%s", this.getUser(), this.getRepo());
                else
                    return String.format("https://bitbucket.org/%s/%s", this.getUser(), this.getRepo());
            case "github":
                // github does not appear to support repo groupings
                return String.format("https://github.com/%s/%s", this.getUser(), this.getRepo());
            case "gitlab":
                if (this.isRepoGroup())
                    return String.format("https://gitlab.com/%s", this.getUser());
                else
                    return String.format("https://gitlab.com/%s/%s", this.getUser(), this.getRepo());

            default:
                return null; // should not happen
        }
    }

    public String getProviderDomain(){
        switch (this.getProvider()){
            case "bitbucket":
                return "bitbucket.org";
            case "github":
                return "github.com";
            case "gitlab":
                return "gitlab.com";
            default:
                return null;
        }
    }
}
