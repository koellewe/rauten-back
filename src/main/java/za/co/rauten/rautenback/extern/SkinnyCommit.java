package za.co.rauten.rautenback.extern;

import java.util.Date;

/**
 * Legit just because I need to return two variables from a method
 */
public class SkinnyCommit {
    private final Date date;
    private final String hash;

    public SkinnyCommit(Date date, String hash) {
        this.date = date;
        this.hash = hash;
    }

    public Date getDate() {
        return date;
    }

    public String getHash() {
        return hash;
    }
}
