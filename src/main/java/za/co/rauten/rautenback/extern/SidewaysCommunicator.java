package za.co.rauten.rautenback.extern;

import org.json.JSONArray;
import org.json.JSONObject;
import za.co.rauten.rautenback.config.Config;
import za.co.rauten.rautenback.data.Project;
import za.co.rauten.rautenback.data.VCSInfo;
import za.co.rauten.rautenback.db.DatabaseManager;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class SidewaysCommunicator {

    private static final String BITBUCKET_API_URL_BASE = "https://api.bitbucket.org/2.0";
    private static final String GITHUB_API_URL_BASE = "https://api.github.com";
    private static final String GITLAB_API_URL_BASE = "https://gitlab.com/api/v4";

    private static final String BITBUCKET_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    private static final String GITHUB_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";
    private static final String GITLAB_DATE_FORMAT = "yyy-MM-dd'T'HH:mm:ss.SSSZZZZZ";

    /**
     * Synchronously make a request to the relevant VCS provider to get:
     *  - project's last updated date (latest commit)
     *  - project's visibility
     * @param project the project to get info of
     */
    public static void updateProject(Project project, DatabaseManager db) throws Exception {
        VCSInfo vcs = project.getVcs();

        switch(vcs.getProvider()){

            case "bitbucket":
                String targetUrl = BITBUCKET_API_URL_BASE + (vcs.isRepoGroup() ?
                        "/repositories/"+vcs.getUser()+"?q=project.key=\""+vcs.getRepo()+"\"" :
                        "/repositories/"+vcs.getUser()+"/"+vcs.getRepo()
                        );

                String respRaw = ClientBuilder.newClient().target(targetUrl).request(MediaType.APPLICATION_JSON)
                        .header("Authorization", genAuthHeader(
                                Config.getInstance().getBitbucketUser()+":"+Config.getInstance().getBitbucketKey()
                        ))
                        .get(String.class);

                JSONObject resp = new JSONObject(respRaw);

                Date latest = null;
                int loc = -1;
                if (vcs.isRepoGroup()) {
                    loc = 0;
                    JSONArray repos = resp.getJSONArray("values");
                    for (int i = 0; i < repos.length(); i++) {
                        JSONObject repo = ((JSONObject) repos.get(i));
                        String repoSlug = repo.getString("slug");
                        SkinnyCommit commit = getLatestCommit(vcs.getProvider(), vcs.getUser(), repoSlug);

                        Date retrieved = commit.getDate();
                        if (latest == null || retrieved.after(latest)){
                            latest = retrieved;
                        }

                        Integer locres = new LoculatorAPI(vcs, commit.getHash(), repoSlug).getLOC();
                        if (locres != null)
                            loc += locres;
                    }
                } else {
                    SkinnyCommit commit = getLatestCommit(vcs.getProvider(), vcs.getUser(), vcs.getRepo());
                    latest = commit.getDate();

                    Integer locres = new LoculatorAPI(vcs, commit.getHash()).getLOC();
                    if (locres != null)
                        loc = locres;
                }

                boolean isPrivate;
                if (vcs.isRepoGroup()){  // look for at least one public repo
                    isPrivate = true;
                    for (Object o : resp.getJSONArray("values"))
                        if (((JSONObject) o).getBoolean("is_private")==false){
                            isPrivate = false; break;
                        }
                }else
                    isPrivate = resp.getBoolean("is_private");

                // run all the updates
                db.updateProjectLastUpdated(project.getId(), latest);
                db.updateProjectVisibility(project.getId(), !isPrivate);
                if (loc != -1)
                    db.updateProjectLoc(project.getId(), loc);
                break;

            case "github": // doesn't have repo groups
                String respRawGH = ClientBuilder.newClient().target(
                        GITHUB_API_URL_BASE+"/repos/"+vcs.getUser()+"/"+vcs.getRepo()
                        ).request(MediaType.APPLICATION_JSON)
                        .header("Authorization", genAuthHeader(
                        Config.getInstance().getGithubUser()+":"+Config.getInstance().getGithubKey()
                        )).get(String.class);

                JSONObject respGH = new JSONObject(respRawGH);

                SkinnyCommit commitGH = getLatestCommit(vcs.getProvider(), vcs.getUser(), vcs.getRepo());
                Integer locGH = new LoculatorAPI(vcs, commitGH.getHash()).getLOC();

                db.updateProjectLastUpdated(project.getId(), commitGH.getDate());
                db.updateProjectVisibility(project.getId(), !respGH.getBoolean("private"));
                if (locGH != null)
                    db.updateProjectLoc(project.getId(), locGH);
                break;


            case "gitlab":
                String respRawGL = ClientBuilder.newClient().target(
                        GITLAB_API_URL_BASE + (vcs.isRepoGroup() ?
                                "/groups/"+vcs.getUser() :
                                "/projects/"+vcs.getUser()+"%2F"+vcs.getRepo() // gitlab's requirements...
                        )
                        ).request(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+Config.getInstance().getGitlabKey())
                        .get(String.class);

                JSONObject respGL = new JSONObject(respRawGL);

                Date latestGL = null;
                int locGL = -1;
                if (vcs.isRepoGroup()) { // iterate through projects tied to group.
                    locGL = 0;
                    for (Object o : respGL.getJSONArray("projects")) {
                        JSONObject repo = ((JSONObject) o);
                        String theRepoSlug = repo.getString("path");
                        SkinnyCommit commit = getLatestCommit(vcs.getProvider(), vcs.getUser(), theRepoSlug);

                        Date retrieved = commit.getDate();
                        if (latestGL == null || retrieved.after(latestGL)) {
                            latestGL = retrieved;
                        }

                        Integer locResGL = new LoculatorAPI(vcs, commit.getHash(), theRepoSlug).getLOC();
                        if (locResGL != null)
                            locGL += locResGL;
                    }
                }else {
                    SkinnyCommit commit = getLatestCommit(vcs.getProvider(), vcs.getUser(), vcs.getRepo());
                    latestGL = commit.getDate();

                    Integer locResGL = new LoculatorAPI(vcs, commit.getHash()).getLOC();
                    if (locResGL != null)
                        locGL = locResGL;
                }

                db.updateProjectLastUpdated(project.getId(), latestGL);
                db.updateProjectVisibility(project.getId(), !respGL.getString("visibility").equals("private"));
                if (locGL != -1)
                    db.updateProjectLoc(project.getId(), locGL);
                break;

        }
    }

    private static String genAuthHeader(String usrPass){
        return "Basic " + DatatypeConverter.printBase64Binary(usrPass.getBytes());
    }

    private static Date parseDate(String dateStr, String format){
        return Date.from(LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(format)).toInstant(ZoneOffset.UTC));
    }

    /**
     * Synchronously gets latest commit info for a repo
     * @param provider vcs provider
     * @param username of the repo owner
     * @param repoSlug of the repo
     * @return latest commit date
     */
    private static SkinnyCommit getLatestCommit(String provider, String username, String repoSlug) throws WebApplicationException {
        String respRaw; JSONObject commit;

        switch (provider) {
            case "bitbucket":
                respRaw = ClientBuilder.newClient().target(
                        BITBUCKET_API_URL_BASE+"/repositories/"+username+"/"+repoSlug+"/commits")
                        .request(MediaType.APPLICATION_JSON)
                        .header("Authorization", genAuthHeader(
                                Config.getInstance().getBitbucketUser()+":"+Config.getInstance().getBitbucketKey()
                        ))
                        .get(String.class);
                JSONObject resp = new JSONObject(respRaw);

                commit = (JSONObject) resp.getJSONArray("values").get(0);
                return new SkinnyCommit(parseDate(commit.getString("date"), BITBUCKET_DATE_FORMAT),
                        commit.getString("hash")
                        );

            case "github":
                respRaw = ClientBuilder.newClient().target(
                        GITHUB_API_URL_BASE + "/repos/"+username+"/"+repoSlug+"/commits"
                        )
                        .request(MediaType.APPLICATION_JSON)
                        .header("Authorization", genAuthHeader(
                                Config.getInstance().getGithubUser()+":"+Config.getInstance().getGithubKey()))
                        .get(String.class);
                JSONArray commits = new JSONArray(respRaw);

                commit = (JSONObject) commits.get(0);
                return new SkinnyCommit(parseDate(
                        commit.getJSONObject("commit").getJSONObject("author").getString("date"), GITHUB_DATE_FORMAT),
                        commit.getString("sha")
                        );

            case "gitlab":
                respRaw = ClientBuilder.newClient().target(
                        GITLAB_API_URL_BASE + "/projects/"+username+"%2F"+repoSlug+"/repository/commits"
                        ).request(MediaType.APPLICATION_JSON)
                        .header("Authorization", "Bearer "+Config.getInstance().getGitlabKey())
                        .get(String.class);
                JSONArray theCommits = new JSONArray(respRaw);

                commit = (JSONObject) theCommits.get(0);
                return new SkinnyCommit(parseDate(commit.getString("committed_date"), GITLAB_DATE_FORMAT),
                        commit.getString("id")
                        );

            default:
                throw new IllegalArgumentException("Unknown provider: " + provider);
        }
    }

}
