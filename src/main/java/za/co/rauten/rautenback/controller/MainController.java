package za.co.rauten.rautenback.controller;

import java.time.Instant;
import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import za.co.rauten.rautenback.RautenbackApplication;
import za.co.rauten.rautenback.data.Project;
import za.co.rauten.rautenback.data.Technology;
import za.co.rauten.rautenback.data.VCSInfo;
import za.co.rauten.rautenback.db.DatabaseManager;
import za.co.rauten.rautenback.extern.SidewaysCommunicator;

@RestController
@CrossOrigin(origins="*")
public class MainController {

    @GetMapping("/")
    public String root() {
       return String.format(
             "This is the rauten.co.za backend \nVersion: %s\n",
             RautenbackApplication.class.getPackage().getImplementationVersion());
    }

    @GetMapping("/error")
    public String error(){
        return "It appears an error has occurred.";
    }

    @GetMapping("/projects")
    public List<Project> projects() throws Exception{
        // automatically sorted by db
    	DatabaseManager db = new DatabaseManager();
    	List<Project> projects = db.getProjects();
    	db.close();
        return projects;
    }

    @GetMapping("/vcsrecords")
    public List<VCSInfo> vcsRecords() throws Exception {
    	DatabaseManager db = new DatabaseManager();
    	List<VCSInfo> vcsRecords = db.getVCSRecords();
    	db.close();
    	return vcsRecords;
    }

    @GetMapping("/techs")
    public List<Technology> techs() throws Exception {
        DatabaseManager db = new DatabaseManager();
        List<Technology> techs = db.getAllTechs();
        db.close();
        return techs;
    }

    @GetMapping("/refreshprojects")
    public ResponseEntity<Void> refreshProjects(@RequestParam String pwd) {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        // do work asynchronously so as to not hold the requester forever
        new Thread(() -> {
            Instant startTime = Instant.now();
            System.out.print(startTime.toString());
            System.out.println(" - Starting project refresh");

            try {
                DatabaseManager db = new DatabaseManager();
                for (Project proj : db.getProjects()) {
                    if (proj.getVcs() != null)
                        try {
                            SidewaysCommunicator.updateProject(proj, db);
                        } catch (WebApplicationException e) {
                            if (e.getResponse().getStatus() / 100 == 5)
                                // try once more
                                SidewaysCommunicator.updateProject(proj, db);
                        }
                }
                db.close();
                long diff = Instant.now().getEpochSecond() - startTime.getEpochSecond();
                long secs = diff % 60;
                long mins = diff / 60;
                System.out.println("Successfully updated all projects (took "+mins+"m"+secs+"s)");
            }catch (Exception e){
                e.printStackTrace();
            }
        }).start();

        return new ResponseEntity<>(HttpStatus.ACCEPTED);

    }

}
